//1 - TEST: status code verification
pm.test('Test name : status is 200',function () {pm.response.to.have.status(200)} );
//2
pm.test("Response time check is below 300", function () {
pm.expect(pm.response.responseTime).to.be.below(300);
});
var Jsonvar = pm.response.json();
pm.environment.set ("address_id",Jsonvar.address_id);



//3 mandatory address fields have the same values as submitted in the request body:
pm.test('mandatory address fields have the same values as submitted in the request body',function () {
    
    var requestBody = JSON.parse(request.data);
     pm.expect(Jsonvar.address1).to.eql(requestBody.address1);
     pm.expect(Jsonvar.postal_code).to.eql(requestBody.postal_code);
     pm.expect(Jsonvar.country_code).to.eql(requestBody.country_code);
     pm.expect(Jsonvar.city).to.eql(requestBody.city);
     pm.expect(Jsonvar.phone).to.eql(requestBody.phone);
     pm.expect(Jsonvar.first_name).to.eql(requestBody.first_name);
     pm.expect(Jsonvar.last_name).to.eql(requestBody.last_name);   
    });

 //4 JSON body validation is ok
 pm.test("JSON body validation is ok", function () {
     pm.response.to.be.ok;
     pm.response.to.be.withBody;
     pm.response.to.be.json;
});   

//5 response header Content-Type presence check
pm.test("Content-Type is present", function () {
    pm.response.to.have.header("content-type");
});