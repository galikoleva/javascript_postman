//tests["Status code is 200"] = responseCode.code === 200;
var data = JSON.parse(responseBody);

//Precondition : 

//1 - TEST: status code verification
pm.test('Test name Gali status is 200',function () {pm.response.to.have.status(200)} );

//2 - Check response value “auth_type” is “registered”
var jsonData = pm.response.json();
pm.test("response value “auth_type” is “registered”", function () {

pm.expect(jsonData.auth_type).to.eql("registered");
});

//3 - TEST: Response time check
pm.test("Response time check is below 300", function () {
    pm.expect(pm.response.responseTime).to.be.below(300);
});
//4 - TEST: JSON body validation

pm.test("JSON body validation is ok", function () {
     pm.response.to.be.ok;
     pm.response.to.be.withBody;
     pm.response.to.be.json;
})

//5 - TEST: Response header check

pm.test("Content-Type and Authorization is present", function () {
    pm.response.to.have.header("Authorization");
    pm.environment.set ("AUTHORIZATION",pm.response.headers.get("Authorization"));
    pm.environment.set ("CUSTOMER_ID",jsonData.customer_id)
    
});