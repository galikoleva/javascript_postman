var Jsonvar = pm.response.json();
//1 - TEST: status code verification
pm.test('Test name : status is 200',function () {pm.response.to.have.status(200)} );
//2 - Response time is less than 220 ms
pm.test("Response time check is below 300", function () {
pm.expect(pm.response.responseTime).to.be.below(400);
});
//3 - "basket_id"/"customer_id"  is the same as the variable with "basket_id"/"customer_id..."
pm.test("basket_id/ customer_id / order_total check and etc till shipments[].shipping_address" , function () {
pm.expect(Jsonvar.basket_id).to.eql(pm.environment.get('BASKET_ID'));
pm.expect(Jsonvar.customer_info.customer_id).to.eql(pm.environment.get('CUSTOMER_ID'));
pm.expect(Jsonvar.order_total).to.be.a('number');
pm.expect(Jsonvar.shipping_total_tax).to.be.a('number');
pm.expect(Jsonvar.tax_total).to.be.a('number');
pm.expect(Jsonvar).to.have.property('coupon_items');
pm.expect(Jsonvar.coupon_items[0].status_code).to.eql('applied');
pm.expect(Jsonvar.coupon_items[0].valid).to.eql(true);
});
//4 JSON body validation
pm.test("JSON body validation is ok", function () {
     pm.response.to.be.ok;
     pm.response.to.be.withBody;
     pm.response.to.be.json;
});
//5 response header Content-Type value is "application/json;charset=UTF-8"
pm.test("Content-Type is application/json;charset=UTF-8", function () {
  pm.expect(pm.response.headers.get('content-type')).to.eql('application/json;charset=UTF-8');
});