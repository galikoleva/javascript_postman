var Jsonvar = pm.response.json();

//1 - TEST: status code verification
pm.test('Test name : status is 200',function () {pm.response.to.have.status(200)} );
//2 - Response time is less than 220 ms
pm.test("Response time check is below 300", function () {
pm.expect(pm.response.responseTime).to.be.below(400);
});
//3 - "payment_method_id" value is "CREDIT_CARD"
pm.test("payment_method_id value is CREDIT_CARD", function () {
pm.expect(Jsonvar.payment_method_id).to.eql('CREDIT_CARD');
});
//4 - JSON body validation is ok
pm.test("JSON body validation is ok", function () {
     pm.response.to.be.ok;
     pm.response.to.be.withBody;
     pm.response.to.be.json;
})
//5 Content-Type value is "application/json;charset=UTF-8
pm.test("Content-Type value is ...charset=UTF-8", function() {
    pm.expect(pm.response.headers.get('content-type')).to.eql('application/json;charset=UTF-8');
})