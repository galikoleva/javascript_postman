var Jsonvar = pm.response.json();
//1 - TEST: status code verification
pm.test('Test name : status is 200',function () {pm.response.to.have.status(200)} );
//2 - Response time is less than 220 ms
pm.test("Response time check is below 300", function () {
pm.expect(pm.response.responseTime).to.be.below(400);
});
//3 - "basket_id"/"customer_id"  is the same as the variable with "basket_id"/"customer_id..."
pm.test("basket_id/ customer_id / order_total check and etc till shipments[].shipping_address" , function () {
pm.expect(Jsonvar.basket_id).to.eql(pm.environment.get('BASKET_ID'));
pm.expect(Jsonvar.customer_info.customer_id).to.eql(pm.environment.get('CUSTOMER_ID'));
pm.expect(Jsonvar.order_total).to.be.a('number');
pm.expect(Jsonvar.shipping_total_tax).to.be.a('number');
});
//4 JSON body validation
pm.test("JSON body validation is ok", function () {
     pm.response.to.be.ok;
     pm.response.to.be.withBody;
     pm.response.to.be.json;
});
//5 response header Content-Type value is "application/json;charset=UTF-8"
pm.test("Content-Type is application/json;charset=UTF-8", function () {
  pm.expect(pm.response.headers.get('content-type')).to.eql('application/json;charset=UTF-8');
});
//6 "shipping_address" is present on the response with all address values as in the saved address
pm.test("shipping_address is present on the response..", function () {
   const savedShipping = Jsonvar.shipments[0].shipping_address.id;
   const savedBilling = Jsonvar.billing_address.id;
   const savedShippingfull = Jsonvar.shipments[0].shipping_address;
   const savedBillingfull = Jsonvar.billing_address;
   const numberOfAttributes = Object.keys(savedBillingfull).length;
   const numberOfAttributesshipping = Object.keys(savedShippingfull).length;
    pm.expect(numberOfAttributes).to.be.at.least(15);
    pm.expect(numberOfAttributesshipping).to.be.at.least(15);
  pm.expect(savedShipping).to.eql(savedBilling);
});


