
var Jsonvar = pm.response.json();
pm.environment.set ("BASKET_ID",Jsonvar.basket_id);
pm.environment.set ("SHIPMENT_ID",Jsonvar.shipments[0].shipment_id);

//1 - TEST: status code verification
pm.test('Test name : status is 200',function () {pm.response.to.have.status(200)} );
//2 - Response time is less than 220 ms
pm.test("Response time check is below 300", function () {
pm.expect(pm.response.responseTime).to.be.below(300);
});
//3 - "basket_id" attribute is present with string value
pm.test("basket_id attribute is present with string value", function () {
pm.expect(Jsonvar.basket_id).to.be.a("string");
});
//4 - JSON body validation is ok
pm.test("JSON body validation is ok", function () {
     pm.response.to.be.ok;
     pm.response.to.be.withBody;
     pm.response.to.be.json;
})
//5 Content-Type value is "application/json;charset=UTF-8
pm.test("Content-Type value is ...charset=UTF-8", function() {
    pm.expect(pm.response.headers.get('content-type')).to.eql('application/json;charset=UTF-8');
})