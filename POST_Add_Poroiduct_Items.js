var Jsonvar = pm.response.json();
//1 - TEST: status code verification
pm.test('Test name : status is 200',function () {pm.response.to.have.status(200)} );
//2 - Response time is less than 220 ms
pm.test("Response time check is below 300", function () {
pm.expect(pm.response.responseTime).to.be.below(300);
});
//3 - "basket_id"/"customer_id"  is the same as the variable with "basket_id"/"customer_id"
pm.test("basket_id/ customer_id / order_total check" , function () {
pm.expect(Jsonvar.basket_id).to.eql(pm.environment.get('BASKET_ID'));
pm.expect(Jsonvar.customer_info.customer_id).to.eql(pm.environment.get('CUSTOMER_ID'));
pm.expect(Jsonvar.order_total).to.be.null;
pm.expect(Jsonvar.product_items[0].product_id).to.eql('VN:000GGG:YB2:L::1:');
pm.expect(Jsonvar.product_items[0].quantity).to.be.oneOf([1,2,3,4,5,6]);
pm.expect(Jsonvar.product_items[0].base_price).to.eql(25.00);
pm.expect(Jsonvar.product_total).to.eql(Jsonvar.product_items[0].price_after_order_discount);
pm.expect(Jsonvar.shipments[0].shipping_address).to.not.be.empty;

//console.log("product_id: " + Jsonvar.product_items[0].price_after_order_discount);
//console.log("product_id: " + Jsonvar.product_total);
});
//4 JSON body validation
pm.test("JSON body validation is ok", function () {
     pm.response.to.be.ok;
     pm.response.to.be.withBody;
     pm.response.to.be.json;
});
//5 response header Content-Type value is "application/json;charset=UTF-8"
pm.test("Content-Type is application/json;charset=UTF-8", function () {
  pm.expect(pm.response.headers.get('content-type')).to.eql('application/json;charset=UTF-8');
});